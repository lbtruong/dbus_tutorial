SRCS_NAME = bluez_dbus

LOCAL_SRCS  = main.c

CC = gcc

CFLAGS+= -O0 -g -Wall -Werror


CPPFLAGS +=$(shell pkg-config glib-2.0 --cflags)

LDLIBS +=$(shell pkg-config glib-2.0 --libs)

#CPPFLAGS +=$(shell pkg-config dbus-1 --cflags)
CPPFLAGS +=$(shell pkg-config --cflags dbus-1)

#LDLIBS +=$(shell pkg-config dbus-1 --libs)
LDLIBS +=$(shell pkg-config --libs dbus-1)


all: $(SRCS_NAME)

$(SRCS_NAME): $(LOCAL_SRCS)
		$(CC) -L. $(CFLAGS) $(CPPFLAGS)  -o $@ $(LOCAL_SRCS) $(LDLIBS)

clean:
	rm -f *.o $(SRCS_NAME)

