#include <stdio.h>
#include <error.h>
#include <dbus/dbus.h>

int main()
{
        DBusError error;
        DBusConnection *conn;
        char *name = "org.freedesktop.btruong";
        dbus_bool_t ret;
        int request_name_reply;

        dbus_error_init (&error);

        conn = dbus_bus_get (DBUS_BUS_SYSTEM, &error);
        if (dbus_error_is_set(&error)) {
                fprintf(stderr, "%s: %s\n", error.name, error.message);
                dbus_error_free(&error);
                return -1;
        }
        
        /* Ask the bus whether our proposed name has owner */
        ret = dbus_bus_name_has_owner(conn, name, &error);
        if (dbus_error_is_set(&error)) {
                fprintf(stderr, "%s: %s\n", error.name, error.message);
                dbus_error_free(&error);
                return -1;
        }

        if (ret == FALSE) {
                printf("Bus name %s doesn't have owner, reserving it...\n", name);

                /* Asks the bus to assign given name to our connection */
                request_name_reply = dbus_bus_request_name (conn, name, DBUS_NAME_FLAG_DO_NOT_QUEUE, &error);
                if (dbus_error_is_set(&error)) {
                        fprintf(stderr, "%s: %s\n", error.name, error.message);
                        dbus_error_free(&error);
                        return -1;
                }

                if (request_name_reply == DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER) {
                        printf("Bus name %s successfully reserverd\n", name);
                        return 0;
                } else {
                        printf("Failed to reserver name %s\n", name);
                        return -1;
                }
        } else {
                printf("%s is already reserverd\n", name);
                return 1;
        }

        return 0;
}
